# Subdirectory Test

This document serves as sufficient proof that index files may be referenced by their parent directories, as a default, even for serving markdown as a static website.

# Table of Contents
  * [Home](../)
  * [Service Provider](../service-provider.md)
  * [Default Test]
  * [Subdirectory Test](test.md)