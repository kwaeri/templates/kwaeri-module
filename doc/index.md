# The Kwaeri Module Standard

Use this repository to obtain the recommended starting point for a kwaeri module.

The kwaeri project works very hard to maintain the latest deps at any time, and with several dozen modules (and counting) - the easiest way to mange this endeavor is to promote an automated approach and to establish and observe a standard heirarchy and structure to our component module project repositories. This includes a standard set of build tools, a standard structure for modules, a standard set of interfaces for module classes, and a standard publisher - subscriber based API for allowing a modular and extensible design; the Service Provider ecosystem. This all comes together into a clearly defined application model where no corners have been cut and countless backsteps have been taken to ensure a sensible, reliable platform where everything fits like a key into it's lock.

When it comes to contributing to kwaeri, the easiest approach is to check out existing facilities. By leveraging the standard starting point, it'll be easy to follow along, borrow, and to recognize the correct approach in accomplishing most goals. Keep in mind that the kwaeri platform is a long-lived project that came from a monolithic - and linear- origin, and has since transformed into a distributed and modular application platform.

This repository also serves as the pipeline sandbox - or playground, really - repository, and is used extensively - not only for maintaining a live reference to the latest standards, but also - for pipeline experimentation and testing.

This home page, and the following linked documentation pages, are all part of the continuously deployed pages environment that any pipeline may leverage in experimentation.