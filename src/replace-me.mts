/**
 * SPDX-PackageName: kwaeri/module
 * SPDX-PackageVersion: 0.1.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// IMPORTS
import {
    NodeKitOptions,
    NodeKitConfigurationBits,
    NodeKitProjectBits,
    NodeKitProjectAuthorBits
} from '@kwaeri/standards-types';
//import { ServiceProvider, Service, ServiceEventBits } from '@kwaeri/service';
//import { GetText } from '@kwaeri/il8n';
import debug from 'debug';


// DEFINES | GLOBALS
const DEBUG = debug( '<kue or nodekit>:<module>' );


export class ReplaceMe /* extends ServiceProvider */ {
    _type: string = "Example Service";

    constructor( /* handler?: ( data: ServiceEventBits ) => void,  configuration?: NodeKitOptions */ ) {
        //super( handler );
    }

    get serviceType() {
        DEBUG( `Return service type '${this._type}'` );

        return this._type;
    }
}

