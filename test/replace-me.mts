/**
 * SPDX-PackageName: kwaeri/module
 * SPDX-PackageVersion: 0.1.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'

// INCLUDES
import * as assert from 'assert';
//import * as fs from 'fs/promises';
//import * as path from 'path';
import { ReplaceMe } from '../src/replace-me.mjs';


// DEFINES
//const VERSION = await import( 'package.json' ).version
const esp = new ReplaceMe();


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    async () => {
                        //const version = JSON.parse( ( await fs.readFile( path.join( './', 'package.json' ), { encoding: "utf8" } ) ) ).version;

                        //console.log( `VERSION: ${version}` );

                        return Promise.resolve(
                            assert.equal( [1,2,3,4].indexOf(4), 3 )
                        );
                    }
                );

            }
        );

    }
);


// Primary tests for the module
describe(
    'ReplaceMe Functionality Test Suite',
    () => {

        describe(
            'Get Service Type Test',
            () => {

                it(
                    'Should return true, indicating that the expected service type was returned.',
                    () => {
                        assert.equal( "Example Service", esp.serviceType );
                    }
                );

            }
        );


    }
);